const redis = require("redis");
const moment = require("moment-timezone");

(function() {
  const timezone = process.env.TIMEZONE;
  const pub = redis.createClient({ host: process.env.REDIS_HOST || "redis" });

  return setInterval(function() {
    const date = new Date();
    const payload = timezone
      ? moment(date)
          .tz(timezone)
          .toString()
      : moment(date).toString();
    pub.publish(`time:${timezone}`, payload);
  }, 200);
})();
