const redis = require("redis");

const options = {
  host: process.env.REDIS_HOST || "redis"
};

const sub = redis.createClient(options);

sub.psubscribe("time:*");

sub.on("pmessage", async (pattern, channel, payload) => {
  console.log(channel, payload);
});
