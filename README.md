Concidering the following docker composition:

```yaml
version: "3.4"
services:
  poster:
    build:
      context: ./poster
    environment:
      - REDIS_HOST=redis
    links:
      - redis

  time:
    build:
      context: ./translator
    environment:
      - REDIS_HOST=redis
      - TIMEZONE=Europe/Paris
    links:
      - redis

  redis:
    image: redis:alpine
```

- the poster service subscribes to a `time:[timezone]` channel from redis and prints in the console whatever data comes in.
- the time service publishes the time in a given timezone every 200ms

The time service relies on a TIMEZONE environment variable. The time service should be dynamically scale up and down based on a dynamic number of TIMEZONES. If 2 timezones are expected, then the composition should have 2 time services running, one for each `TIMEZONE`.

## Problem

Using SaltStack, propose a solution to deploy this stack and dynamically scale up and down the time service on multiple minions.
